'use strict';

const toml = require('toml');
const fs = require('fs');
const get = require('get-value');

/**
 * Represents an interface to the configuration file.
 */
class Config {
    /**
     * Create a new instance of a configuration file.
     * @param {String} fileName Configuration file name
     */
    constructor(fileName) {
        this.env = (process.env.NODE_ENV == 'production') ? 'production' : 'development';

        if (fs.existsSync(fileName)) {
            let s = fs.readFileSync(fileName, { encoding: 'utf-8' });
            this.d = toml.parse(s);
        } else {
            throw new Error('Configuration file does not exist');
        }
    }

    /**
     * Get raw configuration data.
     */
    getRawData() {
        return this.d;
    }

    /**
     * Get a configuration key from the file.
     * @param {String} key Configuration key
     */
    getValue(key) {
        if (get(this.d, `${this.env}.${key}`)) {
            return get(this.d, `${this.env}.${key}`);
        } else {
            return get(this.d, `general.${key}`);
        }
    }

    /**
     * Check if a key exists in the configuration.
     * @param {String} key Configuration key to check for
     */
    hasValue(key) {
        if (get(this.d, `${this.env}.${key}`) || get(this.d, `general.${key}`)) {
            return true;
        } else {
            return false;
        }
    }
}

module.exports.Config = Config;
