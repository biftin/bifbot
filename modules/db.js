'use strict';

const winston = require('winston');
const jsonfile = require('jsonfile');
const path = require('path');
const fs = require('fs');

winston.level = process.env.LOGLEVEL;

const filePath = path.join(process.cwd(), 'db.json');

/**
 * Load the database file.
 */
function loadDBFile() {
    if (fs.existsSync(filePath)) {
        try {
            let data = jsonfile.readFileSync(filePath);
            return data;
        } catch (err) {
            winston.error('database file error:', err);
            process.exit(1);
        }
    } else {
        winston.info('database file does not exist, will be created');
        return {};
    }
}

/**
 * Write data to the database.
 * @param {Object} data Data to write to the database
 */
function saveDBFile(data) {
    jsonfile.writeFile(filePath, data, err => {
        if (err) winston.error('database file error:', err);
    });
}

/**
 * Represents an interface for the database file.
 */
class Database {
    /**
     * Create a new instance of the database interface.
     */
    constructor() {
        this.lastTweet = loadDBFile();
        winston.silly('loaded db data:', this.lastTweet);
    }

    /**
     * Set a user's latest Tweet.
     * @param {String} userId User identifier
     * @param {String} tweetId Tweet identifier
     */
    setLatestTweet(userId, tweetId) {
        this.lastTweet[userId] = tweetId;
        winston.silly('saving data:', this.lastTweet);
        saveDBFile(this.lastTweet);
    }

    /**
     * Get a user's lates Tweet.
     * @param {String} userId User identifier
     */
    getLatestTweet(userId) {
        return this.lastTweet[userId];
    }

    /**
     * Check if a user exists in the database.
     * @param {String} userId User identifier
     */
    hasLatestTweet(userId) {
        if (userId in this.lastTweet) return true;
        else return false;
    }
}

module.exports.Database = Database;
