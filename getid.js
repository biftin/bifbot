#!/usr/bin/env node

/* SPDX-License-Identifier: BSD-3-Clause */

/*
 * This is a helper script.
 * It can be used to retrieve user ids and other information
 * through the Twitter API.
 * It only needs a Twitter handle as an argument.
 * The config files need to be properly set up for this to work.
 */

'use strict';

const pkg = require('./package.json');

const Config = require('./modules/config').Config;
const Twitter = require('twitter');
const ArgumentParser = require('argparse').ArgumentParser;

let config = new Config('./config.toml');

let parser = new ArgumentParser({
    version: pkg.version,
    addHelp: true,
    description: 'Get Twitter user data'
});
parser.addArgument(
    ['-u', '--user'], {
        help: 'User name to get ID for',
        required: true
    }
);
parser.addArgument(
    ['-a', '--all'], {
        help: 'Dump all raw data to the terminal',
        action: 'storeTrue'
    }
);
let args = parser.parseArgs();

if (!config.hasValue('auth'))
    throw new Error('Missing auth configuration');
const authConfig = config.getValue('auth');

let client = new Twitter(authConfig);

let screen_name = args.user;
client.get('users/show', { screen_name })
    .then((d) => {
        if (d.id_str)
            if (args.all)
                console.dir(d);
            else
                console.log(d.id_str);
        else
            throw new Error('No ID given in response');
        process.exit(0);
    })
    .catch((e) => {
        console.error(`Error code ${e[0].code}: ${e[0].message}`);
        process.exit(1);
    });