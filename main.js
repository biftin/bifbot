#!/usr/bin/env node

'use strict';

const pkg = require('./package.json');

const Config = require('./modules/config').Config;
const Twitter = require('twitter');
const winston = require('winston');
const ArgumentParser = require('argparse').ArgumentParser;

let parser = new ArgumentParser({
    version: pkg.version,
    addHelp: true,
    description: 'A Twitter bot that likes Tweets.'
});
parser.addArgument(['-u', '--update'], {
    help: 'Updates the database of latest Tweets.',
    action: 'storeTrue'
});
let args = parser.parseArgs();

let config = null;

try {
    config = new Config('./config.toml');
} catch (e) {
    winston.error('Could not load config file: ', e.message);
    process.exit(1);
}

if (!process.env.LOGLEVEL)
    process.env.LOGLEVEL = config.hasValue('loglevel') ? config.getValue('loglevel') : 'info';
winston.level = process.env.LOGLEVEL;

const db = require('./modules/db');
const userDB = new db.Database();

if (args.update) winston.info('Only updating Tweets...');

const authConfig = config.getValue('auth');

let client = new Twitter(authConfig);

getRateLimitStatus();

const userIds = config.getValue('follow');
if (userIds == null) throw new Error('No user IDs given');

const interval = config.hasValue('interval') ? config.getValue('interval') : 60000;
const include_rts = config.hasValue('include_rts') ? config.getValue('include_rts') : false;

getTweets(userIds, include_rts);
let tweeter = null;
if (!args.update) {
    winston.info(`polling every ${interval}ms`);
    tweeter = setInterval(() => getTweets(userIds, include_rts), interval);
}

process.on('SIGTERM', handleExitSignal);
process.on('SIGINT', handleExitSignal);
process.on('SIGHUP', handleExitSignal);

/**
 * Retrieves the current rate limiting status.
 */
function getRateLimitStatus() {
    client.get('application/rate_limit_status', {}).then(data => {
        winston.verbose(
            'rate limits:',
            'user timelines:',
            data.resources.statuses['/statuses/user_timeline']
        );
    });
}

/**
 * Handle exit signals.
 */
function handleExitSignal() {
    winston.info('exiting');

    if (!args.update) clearInterval(tweeter);
}

/**
 * Retrieves the latest Tweets from the Twitter API.
 * @param {Array} userIds Array of user identifiers
 * @param {Boolean} includeRts Include retweets
 */
function getTweets(userIds, includeRts) {
    winston.verbose('updating tweets');

    userIds.forEach(id => {
        var options;
        if (!userDB.hasLatestTweet(id)) {
            winston.debug('user', id, 'doesn\'t have latest tweet');
            options = {
                user_id: id,
                include_rts: includeRts
            };
        } else
            winston.debug(
                'user:',
                id,
                'has latest tweet:',
                userDB.getLatestTweet(id)
            );
        options = {
            user_id: id,
            include_rts: includeRts,
            since_id: userDB.getLatestTweet(id)
        };

        client
            .get('statuses/user_timeline', options)
            .then(data => handleUserTimeline(data, id))
            .catch(e => {
                winston.error('could not request user timeline:', e);
            });
    });
}

/**
 * Handle data received by the Twitter API.
 * @param {Array} data Array of Tweets.
 * @param {String} id User identifier
 */
function handleUserTimeline(data, id) {
    data.forEach(tweet => {
        likeTweet(tweet.id_str, tweet.user.screen_name, id);
    });
}

/**
 * Like a Tweet and log it to the console.
 * @param {String} tweetId Tweet identifier
 * @param {String} userName User name
 * @param {String} id User identifier
 */
function likeTweet(tweetId, userName, id) {
    if (
        userDB.hasLatestTweet(id) &&
        parseInt(userDB.getLatestTweet(id)) >= parseInt(tweetId)
    ) {
        winston.debug('twitter returned a tweet that was already liked');
        return;
    }

    if (!args.update) {
        winston.info(`liking tweet: ${userName}/${tweetId}`);

        client
            .post('favorites/create', {
                id: tweetId
            })
            .then(() => {
                if (!userDB.hasLatestTweet(id) ||
                    parseInt(tweetId) > parseInt(userDB.getLatestTweet(id))
                )
                    userDB.setLatestTweet(id, tweetId);
            })
            .catch(e => {
                if (e[0].code == 139) {
                    if (!userDB.hasLatestTweet(id) ||
                        parseInt(tweetId) > parseInt(userDB.getLatestTweet(id))
                    )
                        userDB.setLatestTweet(id, tweetId);
                }
                winston.warn(
                    `could not like tweet: ${userName}/${tweetId}:`,
                    e
                );
            });
    } else {
        winston.info(`updating latest tweet: ${userName}/${tweetId}`);
        if (!userDB.hasLatestTweet(id) ||
            parseInt(tweetId) > parseInt(userDB.getLatestTweet(id))
        )
            userDB.setLatestTweet(id, tweetId);
    }
}